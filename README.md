# Artifactory Sidekick

Public Pipe to allow Atlassian's Bitbucket Pipelines builds to obtain credentials to the package management system.

This repository is the open-source part of the project which is required to be publicly available as a pipe.
The actual code and publication of the docker image are in https://bitbucket.org/atlassian/artifactory-sidekick-private/

More information there.
